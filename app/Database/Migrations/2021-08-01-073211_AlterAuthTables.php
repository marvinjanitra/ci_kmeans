<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterAuthTables extends Migration
{
	public function up()
	{
		// Add New Column to table `users`
		$fields = [
			'name'      => ['type' => 'VARCHAR', 'constraint' => 63, 'after' => 'username'],
			'role'       => ['type' => 'ENUM("admin", "user")', 'default' => 'user'],
		];
		$this->forge->addColumn('users', $fields);
	}

	public function down()
	{
		// Drop Column from table `users`
		$this->forge->dropColumn('users', 'name');
		$this->forge->dropColumn('users', 'role');
	}
}
