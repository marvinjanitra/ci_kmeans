<!-- Partner -->
<form id="<?=$modal['id']?>" action="<?=$modal['action']?>" method="post" class="modal" enctype="multipart/form-data">
    <div class="modal-content">
        <h5>Import <?=$modal['name'] ?? ucfirst($modal['model'])?></h5>
        <p><?=$modal['text'] ?? '&nbsp;'?></p>
        <div class="file-field input-field">
            <div class="btn">
                <span>File</span>
                <input type="file" name="file_import" accept=".xlsx, .xls, .csv"/>
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text"/>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" name="import" class="waves-effect waves-grey green-text btn-flat">Import</a>
        <button type="button" class="modal-close waves-effect waves-grey red-text btn-flat">Close</a>
    </div>
</form>