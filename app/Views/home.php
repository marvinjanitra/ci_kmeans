<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php
    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => 'javascript:void(0)'
            ]
        ]
    ]);

    if(user()->role == 'user' || user()->role == 'admin')
    {
?>
        <div class="card center-align z-depth-2" style="margin-top: 15px;">
            <div class="card-content">
                <span class="card-title">
                SISTEM INFORMASI<br>
                K-MEANS CLUSTERING UNTUK MENGELOMPOKAN PENERIMAAN BANTUAN SOSIAL<br>
                BERDASARKAN INFORMASI PEKERJAAN, PENGHASILAN, LISTRIK, KONDISI RUMAH
                </span>
            </div>
            <div class="card-action">
                <p>
                    Sistem Informasi ini digunakan untuk mengelompokan kelompok penduduk berdasarkan informasi pekerjaan, penghasilan, listrik dan kondisi rumah. Dengan adanya sistem informasi ini diharapkan bisa membantu Dinas Sosial dalam mengambil beberapa kebijakan terkait dengan pemberian bantuan sosial pada masyarakat.
                </p>
            </div>
        </div>
<?php
    }
?>
<?= $this->endSection() ?>