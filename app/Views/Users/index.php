<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php
    // Role Permission 
    if(user()->role != 'admin')
    {
        echo "<script>window.location.replace('".base_url('/')."')</script>";
        exit;
    }

    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => base_url('/')
            ],
            [
                'title' => 'Users',
                'link' => 'javascript:void(0)'
            ]
        ]
    ]);

    // Floating Button Setup
    echo view('App\Views\Components/floating-button', [
        'button_items' => [
            [
                'name' => 'Export Data',
                'icon' => 'file_download',
                'class' => 'purple',
                'link' => route_to('users.export')
            ],
            // [
            //     'name' => 'Import Data',
            //     'icon' => 'insert_drive_file',
            //     'class' => 'blue modal-trigger',
            //     'link' => '#modal-users'
            // ],
            [
                'name' => 'Input Manual',
                'icon' => 'add',
                'class' => 'green',
                'link' => route_to('users.create')
            ]
        ]
    ]);
    
    echo view('App\Views\Users/view/table',[
        'users' => $users
    ]);

    echo view('App\Views\Components/modal-import', [
        'modal' => [
            'id' => 'modal-users',
            'model' => 'users',
            'text' => 'Excel Template : <a href="/template/users-template.xlsx" download/>users-template.xlsx</a>',
            'action' => base_url('/users/import'),
        ]
    ]);
?>    
<?= $this->endSection() ?>