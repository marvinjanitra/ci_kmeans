<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php

    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => base_url('/')
            ],
            [
                'title' => 'Data Penduduk',
                'link' => route_to('penduduk.index')
            ],
            [
                'title' => "{$penduduk->nama} ({$penduduk->nik})",
                'link' => 'javascript:void(0)'
            ],
        ]
    ]);

    echo view('App\Views\Penduduk/view/form', [
        'data' => $penduduk
    ]);
?>    
<?= $this->endSection() ?>