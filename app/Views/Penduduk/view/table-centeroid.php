<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Pusat Cluster</span>
            <div class="actions">
            <button class="waves-effect waves-grey green-text btn-flat table-detail-trigger" data-table="<?=$table_centeroid_id?>">Detail</button>
            </div>
        </div>
        <table class="highlight" id="<?=$table_centeroid_id?>">
            <thead>
                <tr>
                    <th>Cluster</th>
                    <th>Kredit</th>
                    <th>Pekerjaan</th>
                    <th>Penghasilan</th>
                    <th>Listrik</th>
                    <th>Kondisi Rumah</th>
                </tr>
            </thead>
            <tbody>
        <?php
            foreach ($centeroid as $i => $value) 
            {
        ?>
                <tr>
                    <td>C<?=$i+1?></td>
                    <td><?= $value['kredit'] ?></td>
                    <td><?= $value['pekerjaan'] ?></td>
                    <td><?= $value['penghasilan'] ?></td>
                    <td><?= $value['listrik'] ?></td>
                    <td><?= $value['kondisi_rumah'] ?></td>
                </tr>
        <?php
            }
        ?>
            </tbody>
        </table>
    </div>
</div>