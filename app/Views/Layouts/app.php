<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KMeans Clustering</title>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!--Import materialize.min.css-->
    <link type="text/css" rel="stylesheet" href="/assets/materialize/css/materialize.min.css"  media="screen,projection"/>

    <style>
        /* Select2 */
        .select2 .selection .select2-selection--single, .select2-container--default .select2-search--dropdown .select2-search__field {
            border-width: 0 0 1px 0 !important;
            border-radius: 0 !important;
            height: 2.05rem;
        }

        .select2-container--default .select2-selection--multiple, .select2-container--default.select2-container--focus .select2-selection--multiple {
            border-width: 0 0 1px 0 !important;
            border-radius: 0 !important;
        }

        .select2-results__option {
            color: #26a69a;
            padding: 8px 16px;
            font-size: 16px;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #eee !important;
            color: #26a69a !important;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #e1e1e1 !important;
        }

        .select2-dropdown {
            border: none !important;
            box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
        }

        .select2-container--default .select2-results__option[role=group] .select2-results__group {
            background-color: #333333;
            color: #fff;
        }

        .select2-container .select2-search--inline .select2-search__field {
            margin-top: 0 !important;
        }

        .select2-container .select2-search--inline .select2-search__field:focus {
            border-bottom: none !important;
            box-shadow: none !important;
        }

        .select2-container .select2-selection--multiple {
            min-height: 2.05rem !important;
        }

        .select2-container--default.select2-container--disabled .select2-selection--single {
            background-color: #ddd !important;
            color: rgba(0,0,0,0.26);
            border-bottom: 1px dotted rgba(0,0,0,0.26);
        }
    </style>

    <?= $this->renderSection('pageStyles') ?>
</head>
<body>
    
    <!-- Start Content -->
    <?= $this->renderSection('app') ?>
    <!-- End Content -->
    
    <!-- Modal Bottom -->
    <div id="modal-bottom-detail" class="modal bottom-sheet">
        <div class="modal-content" id="modal-bottom-content">
        </div>
    </div>
    
    
    <!-- import JQuery -->
    <script type="text/javascript" src="/assets/jquery-3.6.0.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- import materialize.min.js -->
    <script type="text/javascript" src="/assets/materialize/js/materialize.min.js"></script>

    <!-- Message Block -->
    <?= view('App\Views\Layouts\_message_block') ?>

    <script>
    // Custom JS
        $(document).ready(function(){
            // Dropdown Components
            $(".dropdown-trigger").dropdown({
                coverTrigger: false,
                constrainWidth: false,
            });

            // Select Input
            $('select:not(.select2)').formSelect();
            $('.select2').select2({width: "100%"});

            // Floating Button
            $('.fixed-action-btn').floatingActionButton();

            // Tooltip
            $('.tooltipped').tooltip();

            // Modal
            $('.modal').modal();

            // Detail Table
            $('.table-detail-trigger').click(function(e){
                var table_id = $(this).data('table');

                $clone = $('#'+table_id).clone();
                // console.log($clone);
                $('#modal-bottom-content').empty();
                $('#modal-bottom-content').append($clone);
                $('#modal-bottom-content').find('.table-column-action').remove();
                $('#modal-bottom-detail').modal('open');
            });
        })
    </script>

    <?= $this->renderSection('pageScripts') ?>
</body>
</html>