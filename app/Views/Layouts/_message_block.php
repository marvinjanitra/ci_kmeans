<script>
<?php
    echo (session()->has('error')) ? "// Error (Ada)" : "// Error (Tidak)";
    if(session()->has('error'))
    {
?>

    text = "<i class='material-icons'>error_outline</i> &nbsp;&nbsp;<?=session('error')?>"
    M.toast({html: text, classes: 'red lighten-2'})
<?php
        session()->remove('error');
    }
?>

<?php
    echo (session()->has('errors')) ? "// Errors (Ada)" : "// Errors (Tidak)";
    if(session()->has('errors'))
    {
        foreach (session('errors') as $error){ 
?>

            text = "<i class='material-icons'>error_outline</i> &nbsp;&nbsp;<?=$error?>"
            M.toast({html: text, classes: 'red lighten-2'})
<?php
        }
        session()->remove('errors');
    }
?>

<?php
    echo (session()->has('warning')) ? "// Warning (Ada)" : "// Warning (Tidak)";
    if(session()->has('warning'))
    {
?>

    text = "<i class='material-icons'>warning</i> &nbsp;&nbsp;<?=session('warning')?>"
    M.toast({html: text, classes: 'yellow lighten-2'})
<?php
        session()->remove('warning');
    }
?>

<?php
    echo (session()->has('success')) ? "// Success (Ada)" : "// Success (Tidak)";
    if(session()->has('success'))
    {
?>

    text = "<i class='material-icons'>check_circle</i> &nbsp;&nbsp;<?=session('success')?>"
    M.toast({html: text, classes: 'green lighten-2'})
<?php
        session()->remove('success');
    }
?>

<?php
    echo (session()->has('message')) ? "// Message (Ada)" : "// Message (Tidak)";
    if(session()->has('message'))
    {
?>

    text = "<i class='material-icons'>check_circle</i> &nbsp;&nbsp;<?=session('message')?>"
    M.toast({html: text, classes: 'green lighten-2'})
<?php
        session()->remove('message');
    }
?>
</script>